import logging

from pymoney import database as db


logger = logging.getLogger('pymoney_app')


def create(db_name, title, value, place, purpose, description):
    db.create_transaction(db_name, title, value, place, purpose, description)

def list(db_name):
    transactions = db.execute_list_query(db_name, db.select_transactions, 'pymoney_transaction_list')
    return transactions

def detail( db_name, index):
    transaction = db.execute_detail_query(db_name, index)
    return transaction

def update(db_name, index, updated_values):
    db.execute_update_query(db_name, index, updated_values)


def delete(db_name, index):
    db.execute_delete_query(db_name, index)
