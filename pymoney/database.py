import logging
import os
import sqlite3

from sqlite3 import Error

# Variables
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
USER_ACCOUNT_RELATIVE_DIR = 'user_data/accounts'
SQLITE_DATA_FOLDER = os.path.join(BASE_DIR, USER_ACCOUNT_RELATIVE_DIR)

logger = logging.getLogger('pymoney_app')


# Query statments
create_transactions_table = """
    CREATE TABLE IF NOT EXISTS  transactions (
        id  INTEGER PRIMARY KEY AUTOINCREMENT,
        title TEXT NOT NULL,
        value INTEGER,
        place TEXT,
        purpose TEXT,
        description TEXT
    )
    """

select_transactions = """
SELECT id, title, value, place, purpose, description FROM transactions;
"""

def select_specific_transaction(index):
    return f""" SELECT id, title, value, place, purpose, description FROM transactions WHERE id = {index}; """

def update_transaction(index, updated_values):
    title = updated_values[0]
    value = updated_values[1]
    place = updated_values[2]
    purpose = updated_values[3]
    description = updated_values[4]
    return f"""
        UPDATE
            transactions
        SET
            title = '{title}', value = {value}, place = '{place}', purpose = '{purpose}', description = '{description}'
        WHERE
            id = {index};
        """

def delete_transaction(index):
    return f"DELETE FROM transactions WHERE id = {index};"

verify_if_user_account_table_exist = """
    SELECT id, title, value, place, purpose, description FROM transactions WHERE id < 10;
    """
 

# SQLite Database Files
def verify_if_database_exits(db_name):
    return os.path.isfile(os.path.join(SQLITE_DATA_FOLDER, db_name))


def delete_database_file(db_name):
    file_ = os.path.join(SQLITE_DATA_FOLDER, db_name)
    try:
        os.remove(file_)
        logger.info(f"The {db_name} database file was removed.")
    except OSError as e:
        logger.info(f"The {db_name} database file was NOT removed.")


# Connections
def create_connection(db_name):
    path = os.path.join(SQLITE_DATA_FOLDER, db_name)
    connection = None
    try:
        connection = sqlite3.connect(path)
        logger.info(f"Connected to {db_name} database file successful.")
    except Error as e:
        logger.exception(f"The error '{e}' occurred when trying to connect the database.")
        connection.rollback()
        connection.close()
    return connection


# Execute Queries
def execute_query(db_name, query, query_name = "requested"):
    connection = create_connection(db_name)
    cursor = connection.cursor()
    try:
        cursor.execute(query)
        connection.commit()
        logger.info(f"The {query_name} query was executed successfully.")
    except Error as e:
        logger.exception(f"The error '{e}' occurred when executing the \"{query_name}\" on execute_query().")
        connection.rollback()
    finally:
        connection.close()


def execute_query_and_rise_error(db_name, query, query_name = "requested"):
    """Dont logging error. Used to return True or False in select table name."""
    connection = create_connection(db_name)
    cursor = connection.cursor()
    try:
        cursor.execute(query)
        connection.commit()
        logger.info(f"The {query_name} query was executed successfully.")
    except Error as e:
        connection.rollback()
        raise
    finally:
        connection.close()


def execute_list_query(db_name, query, query_name = "requested"):
    connection = create_connection(db_name)
    cursor = connection.cursor()
    record = None
    try:
        cursor.execute(query)
        record = cursor.fetchall()
        return record
    except Error as e:
        logger.exception(f"The error '{e}' occurred when executing \"query_name\" on execute_read_query().")
    finally:
        connection.close()


def execute_detail_query(db_name, index):
    query = select_specific_transaction(index)
    query_name = 'execute_detail_query()'
    connection = create_connection(db_name)
    cursor = connection.cursor()
    record = None
    try:
        cursor.execute(query)
        record = cursor.fetchall()
        if len(record) > 0:
            return record[0]
        else:
            return None
    except Error as e:
        logger.exception(f"The error '{e}' occurred when executing \"query_name\" on execute_detail_query().")
        connection.rollback()
    finally:
        connection.close()


def execute_update_query(db_name, index, updated_values):
    query = update_transaction(index, updated_values)
    query_name = 'execute_update_query()'
    connection = create_connection(db_name)
    cursor = connection.cursor()
    record = None
    try:
        cursor.execute(query)
        connection.commit()
    except Error as e:
        logger.exception(f"The error '{e}' occurred when executing \"query_name\" on execute_detail_query().")
        connection.rollback()
    finally:
        connection.close()


def execute_delete_query(db_name, index):
    query = delete_transaction(index)
    query_name = 'execute_delete_query()'
    connection = create_connection(db_name)
    cursor = connection.cursor()
    record = None
    try:
        cursor.execute(query)
        connection.commit()
    except Error as e:
        logger.exception(f"The error '{e}' occurred when executing \"query_name\" on execute_detail_query().")
        connection.rollback()
    finally:
        connection.close()


# User account tables
def create_user_account_tables(db_name):
    execute_query(db_name, create_transactions_table, 'create_transactions_table')


def verify_if_user_tables_exits(db_name):
    if verify_if_database_exits(db_name):
        try:
            execute_query_and_rise_error(db_name, verify_if_user_account_table_exist, 'verify_if_user_account_table_exist')
            return True
        except Error as e:
            return False
    else:
        return False


# Transactions
def create_transaction(db_name, title, value, place, purpose, description):
    connection = create_connection(db_name)
    query = f"""
                INSERT INTO transactions (title, value, place, purpose, description)
                VALUES ('{title}', {value}, '{place}', '{purpose}', '{description}')
            """
    execute_query(db_name, query, 'create transaction',)
