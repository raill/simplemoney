import logging
import os

from pymoney import database as db

logger = logging.getLogger('pymoney_app')


def verify_if_user_account_exits(user_name):
    return db.verify_if_database_exits(user_name)

def create_user_connection(user_name):
    if verify_if_user_account_exits(user_name):
        db.create_connection(user_name)
    else:
        db.create_user_account_tables(user_name)

def delete_user(user_name):
    db.delete_database_file(user_name)
