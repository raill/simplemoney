import logging
import os
import sys
from tabulate import tabulate

if __name__ == '__main__':
    def include_path_environment():
        """
        Include package level in the python system path.
        """
        BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
        sys.path.append(BASE_DIR)
    include_path_environment()

from pymoney import (
        transaction as tr,
        user_account as ua,
        database as db,
        )
from utility import logger_

logger_conf = logger_.logger_conf

MENU_HEAD_LIST = ('Index', 'Item', 'Price', 'Place', 'Purpose', 'Description')


def main():
    logger_conf()
    logger = logging.getLogger('pymoney_app')
    logger.info('Pymoney app.main() init.')

def list(db_name):
    transactions = tr.list(db_name)
    if len(transactions) == 0:
        print("There are NO transactions on the database.")
    else:
        transactions.insert(0,MENU_HEAD_LIST)
        print(tabulate(transactions))

def show(db_name, index):
    transaction = tr.detail(db_name, index)
    if transaction == None:
        print(f"There is NO transaction with index \"{index}\" on the database.")
    else:
        head_and_transaction=[MENU_HEAD_LIST, transaction]
        print(tabulate(head_and_transaction))

def delete(db_name, index):
    transaction = tr.detail(db_name, index)
    if transaction == None:
        print(f"There no transaction with index \"{index}\" on the database.")
    else:
        tr.delete(db_name, index)

# Actions 
login = ua.create_user_connection
add = tr.create
delete_user = ua.delete_user


if __name__ == '__main__':
    main()
