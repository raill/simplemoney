#!/bin/bash
# Bash Menu Script Example
# 2020.05.04

function print_head()
{
    clear
    echo -e '###########################'
    echo -e '###    Simple Money     ###'
    echo -e '###########################'
    echo
}

function transition_menu_options()
{
    echo
    echo "--> Press ENTER to continue..."
    read
    print_head
}


function login()
{
	python -c "from pymoney.cli import app; app.login(\"$1\")"
}

function add_transaction()
{

        echo -e '### Add transaction ###'
	echo
        echo -e '==> Insert the title: '
        read -p " _ " TITLE
        echo -e '==> Insert the price: '
	read -p " _ " PRICE
        echo -e '==> Insert the place: '
	read -p " _ " PLACE
        echo -e '==> Insert the purpose: '
	read -p " _ " PURPOSE
        echo -e '==> Insert the description: '
	read -p " _ " DESCRIPTION
	echo
	python -c "from pymoney.cli import app; app.add(\"$USERNAME\", \"$TITLE\", $PRICE, \"$PLACE\", \"$PURPOSE\", \"$DESCRIPTION\")"
}

function list_transaction()
{
        echo -e '### List transactions ###'
	echo
	python -c "from pymoney.cli import app; app.list(\"$USERNAME\")"
}

function show_transaction()
{
        echo -e '### Show transaction ###'
	echo
        echo -e '==> Insert the transaction index: '
        read -p " _ " INDEX
	python -c "from pymoney.cli import app; app.show(\"$USERNAME\", $INDEX)"
}

function delete_transaction()
{

        echo -e '### Delete transaction ###'
	echo
        echo -e '==> Insert the transaction index: '
        read -p " _ " INDEX
	echo
        echo '## Do you really want to delete transaction?'
        echo '==> Type "yes" to confirm.'
	read -p " _ " CONFIRMATION

	if [ $CONFIRMATION == "yes" ]
		then 
			python -c "from pymoney.cli import app; app.delete(\"$USERNAME\", $INDEX)"
	else
	    echo "--> The \"$CONFIRMATION\" was not equal \"yes\"."
	fi
}

function delete_user_account()
{

        echo -e '### Remove account ###'
	echo
        echo '## Do you really want to delete account?'
        echo '==> Type "yes" to confirm.'
	read -p " _ " CONFIRMATION

	if [ $CONFIRMATION == "yes" ]
	    then 
		echo
		echo "The user account will be deleted."
	        python -c "from pymoney.cli import app; app.delete_user(\"$USERNAME\")"
    		echo
    		echo "--> Press ENTER to exit..."
    		read
		exit
		 

	else
	    echo
	    echo "--> The \"$CONFIRMATION\" was not equal \"yes\"."
	fi
}

##### Login #####
print_head
echo -e '### Insert the username ###'
read  -p " _ " USERNAME
login $USERNAME

##### Menu  #####
transition_menu_options
echo -e '### Choose an option ###'
echo
PS3='Please enter your choice: '
options=("Add transaction" "List transactions" "Show transaction" "Delete transaction" "Delete account" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Add transaction")
	    transition_menu_options
	    add_transaction
	    transition_menu_options
            ;;
        "List transactions")
	    transition_menu_options
            list_transaction
	    transition_menu_options
            ;;
        "Show transaction")
	    transition_menu_options
            show_transaction
	    transition_menu_options
            ;;
        "Delete transaction")
	    transition_menu_options
            delete_transaction
	    transition_menu_options
            ;;
        "Delete account")
	    transition_menu_options
	    delete_user_account
	    transition_menu_options
            ;;
        "Quit")
            break
            ;;
        *) echo "### invalid option $REPLY";;
    esac
done

