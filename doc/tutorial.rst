#####################
Simple Money Tutorial
#####################

*******
Summary
*******

* `Introduction`_.
* `Requirements`_.
* `Installation`_.
* `Run from shell script`_.
* `Run from a python interactive shell`_.

************
Introduction
************

The **Simple Money** app can record simple financial transactions.

The app is in beta stage, and it may present an unexpected behavior.

************
Requirements
************

* Python version 3.8. Other versions were not tested.
* Python package *tabulate* version 0.8.7. Other versions were not tested.

************
Installation
************

It's recommended to run the app inside a virtualenv since the software are in beta phase.

.. code-block:: 

    python -m venv env_sm

    cd env_sm

    source bin/activate

    pip install -U pip

    git clone https://gitlab.com/raill/simplemoney.git

    cd simplemoney

    pip install -r requirements.txt

*********************
Run from shell script
*********************

Run the shell script
====================

.. code-block:: 

    source start_shell_cli.sh

Create an account
=================

Digit the username, without numbers, special characters, and spaces.

Press the ``Enter`` key to go to menu.

.. code-block:: 

    ###########################
    ###    Simple Money     ###
    ###########################
    
    ### Insert the username ###
     _ foobar
    
    --> Press ENTER to continue...

Create transaction
==================

Digit the option ``1`` and press ``Enter``. Then insert the transaction information.

.. code-block:: 

    ###########################
    ###    Simple Money     ###
    ###########################
    
    ### Choose an option ###
    
    1) Add transaction     3) Show transaction    5) Delete account
    2) List transactions   4) Delete transaction  6) Quit
    Please enter your choice: 1
    
    --> Press ENTER to continue...
    
Enter the transaction information.

.. code-block:: 

    ###########################
    ###    Simple Money     ###
    ###########################
    
    ### Add transaction ###
    
    ==> Insert the title: 
     _ shirt
    ==> Insert the price: 
     _ 1000
    ==> Insert the place: 
     _ store
    ==> Insert the purpose: 
     _ to go to work.
    ==> Insert the description: 
     _ nothing special.
    
    
    --> Press ENTER to continue...

List transactions
=================

Digit the option ``2`` and press ``Enter``. Then insert the transaction information.

.. code-block:: 

    ###########################
    ###    Simple Money     ###
    ###########################
    
    1) Add transaction     3) Show transaction    5) Delete account
    2) List transactions   4) Delete transaction  6) Quit
    Please enter your choice: 2
    
    --> Press ENTER to continue...
    

Then the list will be showed.

.. code-block:: 

    ###########################
    ###    Simple Money     ###
    ###########################
    
    ### List transactions ###
    
    -----  ------  -----  ----------  ----------------  -------------------
    Index  Item    Price  Place       Purpose           Description
    1      shirt   100    Only Store  Wonderful party.  Nothing to mention.
    3      short   100    store       nothing           i just do not know.
    5      asdf    100    asdf        asdf              asdf
    6      title   100    asdf        asdf              asdf
    7      bottle  200    asdf        asdf              asdf
    8      shirt   100    nice store  party             nothing important.
    9      shirt   1000   store       to go to work.    nothing special.
    10     short   800    store       to go to work.    nothing special.
    -----  ------  -----  ----------  ----------------  -------------------
    
    --> Press ENTER to continue...

Show transaction
=================

Digit the option ``3`` and press ``Enter``. Then insert the transaction information.

.. code-block:: 

    ###########################
    ###    Simple Money     ###
    ###########################
    
    ### Choose an option ###
    
    1) Add transaction     3) Show transaction    5) Delete account
    2) List transactions   4) Delete transaction  6) Quit
    Please enter your choice: 3
    
    --> Press ENTER to continue...
    
Insert the transaction index.

If you do not remember the transaction index, use the `List transactions`_ to discover.

.. code-block:: 

    ###########################
    ###    Simple Money     ###
    ###########################
    
    ### Show transaction ###
    
    ==> Insert the transaction index: 
     _ 1

The transaction is showed.

.. code-block:: 

    ###########################
    ###    Simple Money     ###
    ###########################
    
    ### Show transaction ###
    
    ==> Insert the transaction index: 
     _ 1
    -----  -----  -----  ----------  ----------------  -------------------
    Index  Item   Price  Place       Purpose           Description
    1      shirt  100    Only Store  Wonderful party.  Nothing to mention.
    -----  -----  -----  ----------  ----------------  -------------------
    
    --> Press ENTER to continue...

Delete transaction
==================

Digit the option ``4`` and press ``Enter``. 

.. code-block:: 

    ###########################
    ###    Simple Money     ###
    ###########################
    
    ### Choose an option ###
    
    1) Add transaction     3) Show transaction    5) Delete account
    2) List transactions   4) Delete transaction  6) Quit
    Please enter your choice: 4
    
    --> Press ENTER to continue...
    
Insert the transaction index and digit lowercase "yes" to confirm.

If you do not remember the transaction index, use the `List transactions`_ to discover.

.. code-block:: 

    ###########################
    ###    Simple Money     ###
    ###########################
    
    ### Delete transaction ###
    
    ==> Insert the transaction index: 
     _ 1
    
    ## Do you really want to delete transaction?
    ==> Type "yes" to confirm.
     _ yes
    
    --> Press ENTER to continue...

The transaction will be deleted.

Delete account
==============

Digit the option ``5`` and press ``Enter``. 

.. code-block:: 

    ###########################
    ###    Simple Money     ###
    ###########################
    
    ### Choose an option ###
    
    1) Add transaction     3) Show transaction    5) Delete account
    2) List transactions   4) Delete transaction  6) Quit
    Please enter your choice: 5
    
    --> Press ENTER to continue...
    
Insert the transaction index and digit lowercase "yes" to confirm.

If you do not remember the transaction index, use the `List transactions`_ to discover.

.. code-block:: 

    ###########################
    ###    Simple Money     ###
    ###########################

    ### Remove account ###
    
    ## Do you really want to delete account?
    ==> Type "yes" to confirm.
     _ yes
    
    The user account will be deleted.
    
    --> Press ENTER to exit...

The account will be deleted.

***********************************
Run from a python interactive shell
***********************************

To start up, the application should be imported from the interactive python shell. The shell must be call from the parent folder of simplemoney package.

.. code-block::


    $ python

    >>> from pymoney.cli import app 

The development is in the initial stage, but it is possible to use the basic functionalities, which are described in the sections bellow.

Create a user account (sqlite3 database)
========================================

An user account is automatically created after first login.

The CLI (Command Line Interface) is not fully implemented. The confirmation messages are not displayed.

The login name should be a alphanumeric, beginning with a letter, and without spaces. For now, there is no login name verification. So, if a wrong login name are insert, a python code error will appear.

The login name must be quoted.

.. code-block::

    >>> app.login('foobar')

Create some transactions
========================

The fields of a transaction are title, price, place, purpose, and description. The first three are self explanatory. The purpose is for a optional intention register. And description and be any kind of information. Some examples of transactions are displayed bellow:

======= ======= ======= ======= ===================
Title   Price   Place   Purpose Description
======= ======= ======= ======= ===================
shirt   1000    store   party   there was a on sale.
pans    1200    store   party   very expensive.
bread   100     bakery  lunch   Nothing special.
======= ======= ======= ======= ===================

For now, there is no interface to insert the transaction fields one by one. Other restriction is the fields, except title, can be empty, but must be included.

The user account name should be inserted as the first field.

The price must be inserted without the currency separator, and consider two decimal number. For example, ``1000`` will be, on future releases, converted in ``$ 10.00``.

The transactions of the table, considering that they were made by foobar user account, should be included as shown bellow. Note that the price must be unquoted.

.. code-block::

    >>> app.add('foobar', 'shirt', 1000, 'store', 'party', 'there was a onsale.')
    >>> app.add('foobar', 'pans', 1200, 'store', 'party', 'very expensive.')
    >>> app.add('foobar', 'bread', 100, 'barkery', 'lunch', 'Nothing special.')

List some transactions
======================

To list the transactions, just run the code:

.. code-block::

    >>> app.list('foobar')

    -----  -----  -----  -------  -------  -------------------
    Index  Item   Price  Place    Purpose  Description
    1      shirt  1000   store    party    there was a onsale.
    2      pans   1200   store    party    very expensive.
    3      bread  100    barkery  lunch    Nothing special.
    -----  -----  -----  -------  -------  -------------------

Show a transaction
==================

To show a transaction, the user name and index number need to be provided. The index number could be obtained from a list mentioned in the previous section.

.. code-block::

    >>> app.show('foobar', 2)

Delete a transaction
====================

To delete a transaction, the user name and index number need to be provided. The index number could be obtained from a list mentioned in the previous section.

The operation is irreversible.

.. code-block::

    >>> app.delete('foobar', 1)

To verify the remove, list the transactions again.

.. code-block::

    >>> app.list('foobar')

    -----  -----  -----  -------  -------  -------------------
    Index  Item   Price  Place    Purpose  Description
    2      pans   1200   store    party    very expensive.
    3      bread  100    barkery  lunch    Nothing special.
    -----  -----  -----  -------  -------  -------------------

Delete user account (sqlite3 database)
======================================

To delete a account, the user name to be provided.

The operation is irreversible.

.. code-block::

    >>> app.delete_user('foobar')

