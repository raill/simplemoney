from pymoney import transaction as tr

FIVE_TRANSACTIONS = (
    ('shirt', 1000,'store', 'to go party', 'some desprition'),
    ('short', 800, 'store', 'to go party', 'another desprition'),
    ('shoes', 1500, 'store', 'to go party', 'third desprition'),
    ('bottle', 400, 'bar', 'make some snack', 'just water'),
    ('bread', 100, 'bakery', 'make some snack', 'some description'),
    )


def create_five_transactions(db_name):

    for transaction in FIVE_TRANSACTIONS:
        tr.create(
            db_name,
            transaction[0],
            transaction[1],
            transaction[2],
            transaction[3],
            transaction[4],
            )
