import os
import logging

def logger_conf():
    LOGGING_FOLDER = os.path.dirname(os.path.abspath(__file__))
    LOGGING_FILE_NAME = 'pymoney_app.log'
    LOGGING_FILE = os.path.join(LOGGING_FOLDER,LOGGING_FILE_NAME)
    FORMAT = '%(asctime)s %(levelname)s %(filename)s %(name)s - %(message)s'
    DATE_FORMAT='%Y.%m.%d_%H.%M.%S'

    logger = logging.getLogger('pymoney_app')

    formatter = logging.Formatter(FORMAT)

    file_handler = logging.FileHandler(LOGGING_FILE)
    file_handler.setFormatter(formatter)

    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter)
    stream_handler.setLevel(logging.WARNING)

    logger.setLevel(logging.DEBUG)
    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)

    logger.debug('logger_config() loaded.')
