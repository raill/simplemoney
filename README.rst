############
Simple Money
############

A simple python app for registering financial transactions.

A `tutorial <doc/tutorial.rst>`_ are available for helping the first use.

*******
Summary
*******

********
Releases
********

Release v0.2.0 Beta
===================

* Included a shell script with a basic CLI (Command Line Interface).

Release v0.1.0 Beta
===================

* Basic operations:

  * database: create and delete sqlite3 database.
  * transaction: create, list, detail and delete transactions.

********
Road map
********

#. Show a list of a available sqlite3 files.
#. Return message for a nonexistence database (the app displays an exception on current version).
#. Keep the database name between transactions.
#. Export transactions in cvs format.
#. Create backup and restore function for transactions.
#. Use python Sphinx to generate documentation.
#. Implement a CLI (Command Line Interface).
#. Implement a GUI (Graphical User Interface).
