import logging
import os

from steps.transactions import SQLITE_BEHAVE_TEST_DB_NAME
from pymoney.database import SQLITE_DATA_FOLDER
from utility import logger_

logger = logging.getLogger('pymoney_behave_logger')
logger_conf = logger_.logger_conf

def logger_config():
    # behave logger config
    LOGGING_FOLDER = os.path.dirname(os.path.abspath(__file__))
    LOGGING_FILE_NAME = 'pymoney_behave.log'
    LOGGING_FILE = os.path.join(LOGGING_FOLDER,LOGGING_FILE_NAME)
    FORMAT = '%(asctime)s %(levelname)s %(filename)s %(name)s - %(message)s'
    DATE_FORMAT='%Y.%m.%d_%H.%M.%S'

    formatter = logging.Formatter(FORMAT)

    file_handler = logging.FileHandler(LOGGING_FILE)
    file_handler.setFormatter(formatter)

    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter)
    stream_handler.setLevel(logging.WARNING)

    logger.setLevel(logging.DEBUG)
    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)

    logger.info('Function logger_config loaded.')

    # pymoney logger config
    logger_conf()

def remove_sqlite_files():
    files = os.listdir(SQLITE_DATA_FOLDER)
    if len(files) > 0:
        for filename in files:
            try:
                os.remove(os.path.join(SQLITE_DATA_FOLDER, filename))
                logger.info(f"The \"{filename}\" database file was removed.")
            except OSError as e:
                logger.exception(f"The \"{filename}\" database file was NOT removed.")

def before_all(context):
    logger_config()
    logger.debug('Logger session started.')

def after_scenario(context,scenario):
    remove_sqlite_files()

def after_all(context):
    remove_sqlite_files()
    logger.debug('Logger session finished.')
