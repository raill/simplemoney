from behave import given, when, then

from tests import database as test_db
from pymoney import database as db, user_account as ua

@given('an user named "{user_name}" will try the software the first time.')
def step_connect_to_db_first_time(context, user_name):
    context.user_name = user_name
    assert ua.verify_if_user_account_exits(user_name) == False, 'The sqlite database for tests already exists.'

@given('an user named "{user_name}" has created an user account.')
def step_insert_the_username(context, user_name):
    context.user_name = user_name
    ua.create_user_connection(context.user_name)

@when('he run the python script app.login(username).')
def step_insert_the_username(context):
    ua.create_user_connection(context.user_name)

@when('he run the python script app.delete(username).')
def step_insert_the_username(context):
    ua.delete_user(context.user_name)

@then('the sqlite database with the same name of the user is created.')
def step_the_sqlite_database_and_the_transaction_are_created(context):
    assert ua.verify_if_user_account_exits(context.user_name) == True, 'The sqlite database with the same name of the user was NOT created.'

@then('the transaction table is created.')
def step_the_sqlite_database_and_the_transaction_are_created(context):
    db.create_user_account_tables(context.user_name)
    assert db.verify_if_user_tables_exits(context.user_name) == True, 'The user accounts tables was NOT created.'

@then('the sqlite database is deleted.')
def step_he_sqlite_database_and_the_transaction_are_created(context):
    assert ua.verify_if_user_account_exits(context.user_name) == False, 'The user account of {context.user_name} was NOT removed.'
