import logging
import os
import sqlite3

from behave import given, when, then
from sqlite3 import Error

from pymoney import (
        database as db,
        transaction as tr,
        user_account as ua
        )


logger = logging.getLogger('pymoney_behave_logger')


from pymoney.database import SQLITE_DATA_FOLDER
SQLITE_BEHAVE_TEST_DB_NAME = 'behave_sqlite_teste.db'

SQLITE_BEHAVE_TEST_DB = os.path.join(SQLITE_DATA_FOLDER, SQLITE_BEHAVE_TEST_DB_NAME)
TRANSACTIONS = [
        ('bottle', 800, 'bar', 'drink', 'kk'),
        ('shirt', 1300, 'store', 'dress', 'kk'),
        ('short', 900, 'store', 'dress', 'kk'),
        ('bread', 200, 'market', 'eat', 'kk'),
        ('water', 500, 'market', 'eat', 'kk'),
        ]

INDEXED_TRANSACTIONS = [
        (1, 'bottle', 800, 'bar', 'drink', 'kk'),
        (2, 'shirt', 1300, 'store', 'dress', 'kk'),
        (3, 'short', 900, 'store', 'dress', 'kk'),
        (4, 'bread', 200, 'market', 'eat', 'kk'),
        (5, 'water', 500, 'market', 'eat', 'kk')
        ]

@given('he register some transactions.')
def step_connect_to_db(context):
    ua.create_user_connection(SQLITE_BEHAVE_TEST_DB)
    for list in TRANSACTIONS:
        tr.create(
                SQLITE_BEHAVE_TEST_DB,
                list[0],
                list[1],
                list[2],
                list[3],
                list[4],
                )
    assert os.path.isfile(SQLITE_BEHAVE_TEST_DB) == True, f"The {SQLITE_BEHAVE_TEST_DB} does NOT exist"


@given('an user connect to a database.')
def step_connect_to_db(context):
    ua.create_user_connection(SQLITE_BEHAVE_TEST_DB)
    assert os.path.isfile(SQLITE_BEHAVE_TEST_DB) == True, f"The {SQLITE_BEHAVE_TEST_DB} does NOT exist"


@when('he insert the fields "{title}", "{value}", "{local}", "{purpose}", and "{text}".')
def step_insert_the_fields(context, title, value, local, purpose, text):
    context.title = title
    context.amount = value 
    context.place = local 
    context.purpose = purpose 
    context.description = text 

@when('he request the list of transactions.')
def step_insert_the_fields(context):
    context.list_of_transactions = tr.list(SQLITE_BEHAVE_TEST_DB)

@when('he request to see the transaction of index "{index}".')
def step_insert_the_fields(context, index):
    transaction_name = "transaction_" + index
    context.transaction_name = tr.detail(SQLITE_BEHAVE_TEST_DB, index)

@when('he request to update one transaction.')
def step_insert_the_fields(context):
    update = ('bread', 150, 'barkery', 'eat', 'yy')
    context.index = 4
    tr.update(SQLITE_BEHAVE_TEST_DB, context.index, update)

@when('he request to delete one transaction.')
def step_insert_the_fields(context):
    context.index = 4
    tr.delete(SQLITE_BEHAVE_TEST_DB, context.index)

@then('the transaction is sent.')
def step_register_transaction(context):
    tr.create(
        SQLITE_BEHAVE_TEST_DB_NAME,
        context.title,
        context.amount,
        context.place,
        context.purpose,
        context.description,
        )

@then('the transaction list is displayed.')
def step_list_transactions(context):
    assert context.list_of_transactions == INDEXED_TRANSACTIONS, f"The list of trasanctions {context.list_of_transactions} is diferent from the list created {INDEXED_TRANSACTIONS}."

@then('the transaction of index "{index}" is displayed.')
def step_detail_transaction(context, index):
    context.index = index
    transaction_name = "transaction_" + index
    assert context.transaction_name is not None, f"The detail transaction function are return a None value."
    assert context.transaction_name is not "", f"The detail transaction function are return a empty value."

@then('the transaction is updated.')
def step_check_if_transaction_was_updated(context):
    transaction_updated = tr.detail(SQLITE_BEHAVE_TEST_DB, context.index)
    indexed_update = (4, 'bread', 150, 'barkery', 'eat', 'yy')
    assert indexed_update == transaction_updated; f"The update data \"{indexed_update}\" was not insert in the table item \"{transaction_updated}\"."

@then('the transaction is deleted.')
def step_check_if_transaction_was_deleted(context):
    transaction_deleted = tr.detail(SQLITE_BEHAVE_TEST_DB, context.index)
    assert None == transaction_deleted; f"The transaction was not deleted from table."
