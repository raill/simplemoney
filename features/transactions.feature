Feature: Create, list, show, update, and delete a transaction.
  The user resgister transactions from interactive python shell.
  The fields are title, value, place, purpose, description. 
  >>> import app; app.add( ... );
  >>> app.update( ... ); app.list( ... ); app.detail( ... );

    Scenario Outline: Register a simple payment.
       Given an user connect to a database.
        When he insert the fields "<title>", "<value>", "<local>", "<purpose>", and "<text>".
        Then the transaction is sent.
      Examples:
        | title    | value | local   | purpose | text        |
	| bottle   | 1000  | markt   | food    | just some.. |
        | shirt    | 0150  | store   | cloth   | I wanna ... |

    Scenario: List transactions.
       Given an user connect to a database.
         And he register some transactions.
        When he request the list of transactions.
        Then the transaction list is displayed.

    Scenario Outline: Show transaction.
       Given an user connect to a database.
         And he register some transactions.
        When he request to see the transaction of index "<index>".
        Then the transaction of index "<index>" is displayed.
      Examples:
        | index  |
	| 1   |
	| 4   |
    
    Scenario: Update transaction.
       Given an user connect to a database.
         And he register some transactions.
        When he request to update one transaction.
        Then the transaction is updated.

    @wip
    Scenario: Delete transaction.
       Given an user connect to a database.
         And he register some transactions.
        When he request to delete one transaction.
        Then the transaction is deleted.
