Feature: Create and remove user accounts.
  The user start the system from interactive python shell.
  >>> import app; app.login('foobar'); app.delete('foobar');

  Scenario Outline: Accessing pymoney first time.
    An user first run the python script, a sqlite database and a transaction table should be created.
    The database file name should be the username, and it should be unique.

     Given an user named "<user_name>" will try the software the first time.
      When he run the python script app.login(username).
      Then the sqlite database with the same name of the user is created.
       And the transaction table is created.

      Examples:
        | user_name | 
	| foo       | 
	| bar       | 
	| namber3   | 
	| 1234      | 
	#| one side  | 
	#| sadf*     | 

  Scenario Outline: Deleting the user account.
    After the user run the python script, a sqlite database should be removed.

     Given an user named "<user_name>" has created an user account.
      When he run the python script app.delete(username).
      Then the sqlite database is deleted.

      Examples:
        | user_name | 
	| foo       | 
	| bar       | 
	| namber3   | 
	| 1234      | 
	#| one side  | 
	#| sadf*     | 
