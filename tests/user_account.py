import logging
import os
import unittest

if __name__ == '__main__':
    from environment_setup import include_path_environment
    include_path_environment()

import utils
from pymoney import database as db, user_account as ua


logger = logging.getLogger('pymoney_unittest')
remove_database_files = utils.remove_database_files


class TestUserAccount(unittest.TestCase):

    def setUp(self):
        remove_database_files(self)

    def tearDown(self):
        remove_database_files(self)
        
    def test_if_an_existent_user_account_return_true(self):
        username = 'tester_user'
        ua.create_user_connection(username)
        self.assertTrue(
                ua.verify_if_user_account_exits(username),
                f"The function returns \"False\" to an existent user.",
                )
                
    def test_if_a_inexistent_user_account_return_false(self):
        username = 'tester_user'
        ua.create_user_connection(username)
        ua.delete_user(username)
        self.assertFalse(
                ua.verify_if_user_account_exits(username),
                f"The function returns \"True\" to a deleted user.",
                )

    def test_create_new_user_connection(self):
        username = 'new_tester_user'
        self.assertFalse(
                ua.verify_if_user_account_exits(username),
                f"The new user has already registered in the system."
                )
        ua.create_user_connection(username)

    def test_create_connection_for_an_existent_user(self):
        username = 'tester_user'
        ua.create_user_connection(username)
        self.assertTrue(
                ua.verify_if_user_account_exits(username),
                f"The function returns \"False\" to an existent user.",
                )
        ua.create_user_connection(username)

    def test_delete_an_existent_user(self):
        username = 'tester_user'
        ua.create_user_connection(username)
        self.assertTrue(
                ua.verify_if_user_account_exits(username),
                f"The function returns False to an existent user.",
                )
        ua.delete_user(username)
        self.assertFalse(
                ua.verify_if_user_account_exits(username),
                f"The new user has already registered in the system."
                )

    def test_delete_an_inexistent_user(self):
        username = 'inexistent_user'
        self.assertFalse(
                ua.verify_if_user_account_exits(username),
                f"The function returns False to an existent user.",
                )
        with self.assertLogs('pymoney_app', level='INFO') as cm:
           ua.delete_user(username)
        self.assertEqual(cm.output, ['INFO:pymoney_app:The inexistent_user database file was NOT removed.',]) 

if __name__ == '__main__':
    unittest.main()
