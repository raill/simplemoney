import unittest
import os
import sys

from tabulate import tabulate

if __name__ == '__main__':
    from environment_setup import include_path_environment
    include_path_environment()

from tests import utils
from pymoney import transaction as tr, database as db
from utility import transaction as utr


SQLITE_TEST_DB_NAME = 'sqlite_test.db'
remove_database_files = utils.remove_database_files


class TestTransaction(unittest.TestCase):
    def setUp(self):
        remove_database_files(self)

    def tearDown(self):
        remove_database_files(self)

    #@unittest.skip("skip")
    def test_can_create_transaction(self,db_name=SQLITE_TEST_DB_NAME):
        db.create_connection(db_name)
        db.create_user_account_tables(db_name),
        tr.create(
                db_name,
                'shirt', 
                100, 
                'store',
                'personal use.',
                'on sale',
                )
        transaction = db.execute_detail_query(db_name, 1) 
        title = transaction[1]
        self.assertEqual('shirt', title, f"The title should be \"shirt\", and NOT {title}.")

    #@unittest.skip("skip")
    def test_can_list_transactions(self,db_name=SQLITE_TEST_DB_NAME):
        db.create_connection(db_name)
        db.create_user_account_tables(db_name),
        utr.create_five_transactions(db_name)
        transactions = tr.list(db_name)
        number_of_transactions = len(transactions)
        self.assertEqual(number_of_transactions, 5, f"The number of transactions created by create_five_transactions are 5, not \"{number_of_transactions}\".")
        self.assertEqual(transactions[0][1], 'shirt', f"The transactions were created by create_five_transactions functions.")
        self.assertEqual(transactions[2][2], 1500, f"The transactions were created by create_five_transactions functions.")
        self.assertEqual(transactions[2][3], 'store', f"The transactions were created by create_five_transactions functions.")
        self.assertEqual(transactions[4][5], 'some description', f"The transactions were created by create_five_transactions functions.")

    #@unittest.skip("skip")
    def test_can_detail_a_transaction(self,db_name=SQLITE_TEST_DB_NAME):
        db.create_connection(db_name)
        db.create_user_account_tables(db_name),
        utr.create_five_transactions(db_name)
        transaction_1 = tr.detail(SQLITE_TEST_DB_NAME, 1)
        transaction_3 = tr.detail(SQLITE_TEST_DB_NAME, 3)
        transaction_5 = tr.detail(SQLITE_TEST_DB_NAME, 5)
        self.assertEqual(transaction_1, (1, 'shirt', 1000,'store', 'to go party', 'some desprition'), f"The transaction {transaction_1} are different from {transaction_1}.")
        self.assertEqual(transaction_3, (3, 'shoes', 1500, 'store', 'to go party', 'third desprition'), f"The transaction {transaction_3} are different from {transaction_3}.")
        self.assertEqual(transaction_5, (5, 'bread', 100, 'bakery', 'make some snack', 'some description'), f"The transaction {transaction_5} are different from {transaction_5}.")

    #@unittest.skip("skip")
    def test_can_update_transaction(self,db_name=SQLITE_TEST_DB_NAME):
        db.create_connection(db_name)
        db.create_user_account_tables(db_name),
        utr.create_five_transactions(db_name)
        updated_values_index_1 = ('shirt', 10.00,'store', 'to go party', 'some desprition'    )
        updated_values_index_3 = ('shoes', 15.00, 'store', 'to go party', 'third desprition')
        updated_values_index_5 = ('bread', 1.00, 'bakery', 'make some snack', 'some description')
        tr.update(db_name, 1, updated_values_index_1)

    #@unittest.skip("skip")
    def test_can_delete_transaction(self,db_name=SQLITE_TEST_DB_NAME):
        db.create_connection(db_name)
        db.create_user_account_tables(db_name),
        utr.create_five_transactions(db_name)
        tr.delete(db_name, 4)
        transaction_4_deleted = tr.detail(SQLITE_TEST_DB_NAME, 4)
        transaction_3 = tr.detail(SQLITE_TEST_DB_NAME, 3)
        transaction_5 = tr.detail(SQLITE_TEST_DB_NAME, 5)
        self.assertIsNone(transaction_4_deleted)
        self.assertIsNotNone(transaction_3)
        self.assertIsNotNone(transaction_5)


if __name__ == '__main__':
    unittest.main()

