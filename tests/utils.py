import logging
import os

from pymoney import database as db


SQLITE_DATA_FOLDER = db.SQLITE_DATA_FOLDER
SQLITE_TEST_DB_NAME = 'sqlite_test.db'
logger = logging.getLogger('pymoney_unittest')


def remove_database_files(self):
    files = os.listdir(SQLITE_DATA_FOLDER)
    if len(files) > 0:
        for filename in files:
            try:
                os.remove(os.path.join(SQLITE_DATA_FOLDER, filename))
                logger.info(f"The \"{filename}\" database file was removed.")
            except OSError as e:
                logger.exception(f"The \"{filename}\" database file was NOT removed.")
