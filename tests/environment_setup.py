import os
import sys

def include_path_environment():
    """
    Include package level in the python system path.
    """
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    sys.path.append(BASE_DIR)

def show_path_environment():
    print(sys.path)
