import logging
import os
import sys
import unittest

if __name__ == '__main__':
    from environment_setup import include_path_environment
    include_path_environment()

from tests import utils
from pymoney import database as db, user_account as ua
from utility import transaction as utr


SQLITE_TEST_DB_NAME = 'sqlite_test.db'


logger = logging.getLogger('pymoney_unittest')
remove_database_files = utils.remove_database_files


def base_dir():
    """
    This function return the path of base directory of the pymoney app.
    The base directory is NOT imported from pymoney.database module to assure the unittest is NOT replicating the error from that module.
    """
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    USER_ACCOUNT_RELATIVE_DIR = 'user_data/accounts'
    return os.path.join(BASE_DIR, USER_ACCOUNT_RELATIVE_DIR)


#@unittest.skip('not wip')
class TestSQLiteDatabaseFile(unittest.TestCase):
    """
    This class tests the creation, deletion and connection to the sqlite database file used to store the user data.
    """
    def setUp(self):
        remove_database_files(self)

    def tearDown(self):
        remove_database_files(self)

    def test_return_false_for_inexistent_sqlite_database(self):
        """
        It tests if db.verify_if_database_exits(db_name) returns False for a database that does NOT exist in the user data folder.
        """
        db_name = 'test_verify_if_db_exists'
        SQLITE_DATA_FOLDER = base_dir()
        is_file_exist = os.path.isfile(os.path.join(SQLITE_DATA_FOLDER, db_name))
        self.assertFalse(
                is_file_exist,
                f"The \"is_file_exist\" is return \"True\" for an existent database."
                )
        self.assertFalse(
                db.verify_if_database_exits(db_name),
                f"The \"db.verify_if_database_exits()\" is return \"True\" for an inexistent database."
                )

    def test_return_true_for_an_existent_sqlite_database(self):
        """
        It tests if db.verify_if_database_exits(db_name) returns True for a database created in the user data folder.
        """
        db_name = 'test_verify_if_db_exists'
        SQLITE_DATA_FOLDER = base_dir()
        db.create_connection(db_name)
        is_file_exist = os.path.isfile(os.path.join(SQLITE_DATA_FOLDER, db_name))
        self.assertTrue(
                is_file_exist,
                f"The database file {db_name} was not created by \"db.create_connection()\".",
                )
        self.assertTrue(
                db.verify_if_database_exits(db_name),
                f"The \"db.verify_if_database_exits()\" is return \"False\" for an existent database file."
                )

    def test_create_a_new_sqlite_database(self, db_name=SQLITE_TEST_DB_NAME):
        self.assertFalse(
                db.verify_if_database_exits(db_name),
                f"The {db_name} database file already exists."
                )
        db.create_connection(db_name)
        self.assertTrue(
                db.verify_if_database_exits(db_name),
                f"The {db_name} database file was NOT created."
                )

    def test_connect_an_existent_sqlite_database(self, db_name=SQLITE_TEST_DB_NAME):
        db.create_connection(db_name)
        self.assertTrue(
                db.verify_if_database_exits(db_name),
                f"The {db_name} database file was NOT created."
                )
        with self.assertLogs('pymoney_app', level='INFO') as cm:
            db.create_connection(db_name)
            self.assertEqual(cm.output, ['INFO:pymoney_app:Connected to sqlite_test.db database file successful.',]) 

    def test_delete_an_existent_sqlite_database(self, db_name=SQLITE_TEST_DB_NAME):
        db.create_connection(db_name)
        db.delete_database_file(db_name)
        self.assertFalse(
                db.verify_if_database_exits(db_name),
                f"The {db_name} database file was NOT deleted.",
                )


#@unittest.skip('not wip')
class TestSQLiteAccountTables(unittest.TestCase):
    """
    This class tests the creation of database tables used to store the user data.
    """
    def setUp(self):
        remove_database_files(self)

    def tearDown(self):
        remove_database_files(self)

    def test_can_create_the_account_tables_exist(self,db_name=SQLITE_TEST_DB_NAME):
        db.create_connection(db_name)
        db.create_user_account_tables(db_name)

    def test_return_false_when_the_account_tables_does_not_exist(self,db_name=SQLITE_TEST_DB_NAME):
        self.assertFalse(
            db.verify_if_database_exits(db_name),
            f"The \"verify_if_database_exits\" is returning a \"True\" for an inexistent database tables."
            )
        self.assertFalse(
            db.verify_if_user_tables_exits(db_name),
            f"The \"db.verify_if_tables_exits()\" is return \"True\" for an inexistent database tables."
            )

    def test_return_true_when_the_account_tables_exist(self,db_name=SQLITE_TEST_DB_NAME):
        db.create_user_account_tables(db_name),
        self.assertTrue(
            db.verify_if_database_exits(db_name=SQLITE_TEST_DB_NAME),
            f"The \"verify_if_database_exits\" is returning a \"False\" for an existent database tables."
            )
        self.assertTrue(
            db.verify_if_user_tables_exits(db_name),
            f"The \"db.verify_if_tables_exits()\" is return \"False\" for an existent database tables."
            )


#@unittest.skip('not wip')
class TestSQLiteQueries(unittest.TestCase):
    """
    This class tests the execution of database queries. 
    """
    def setUp(self):
        remove_database_files(self)

    def tearDown(self):
        remove_database_files(self)

    #@unittest.skip('not wip')
    def test_can_excute_list_of_transactions(self,db_name=SQLITE_TEST_DB_NAME):
        db.create_connection(db_name)
        db.create_user_account_tables(db_name)
        utr.create_five_transactions(db_name)
        transactions = db.execute_list_query(db_name, db.select_transactions, 'test_can_execute_list_of_transactions')
        number_of_transactions = len(transactions)
        self.assertEqual(number_of_transactions, 5, f"The number of transactions created by create_five_transactions are 5, not \"{number_of_transactions}\".")
        self.assertEqual(transactions[0][1], 'shirt', f"The transactions were created by create_five_transactions functions.")
        self.assertEqual(transactions[2][2], 1500, f"The transactions were created by create_five_transactions functions.")
        self.assertEqual(transactions[2][3], 'store', f"The transactions were created by create_five_transactions functions.")
        self.assertEqual(transactions[4][5], 'some description', f"The transactions were created by create_five_transactions functions.")

    #@unittest.skip('not wip')
    def test_can_execute_detail_transaction(self,db_name=SQLITE_TEST_DB_NAME):
        db.create_connection(db_name)
        db.create_user_account_tables(db_name)
        utr.create_five_transactions(db_name)
        transaction_1 = db.execute_detail_query(db_name, 1)
        transaction_4 = db.execute_detail_query(db_name, 4)
        transaction_5 = db.execute_detail_query(db_name, 5)
        self.assertEqual(transaction_1,(1, 'shirt', 1000,'store', 'to go party', 'some desprition'), f"The detail transaction {transaction_1} is different from the created.")
        self.assertEqual(transaction_4,(4,'bottle', 400, 'bar', 'make some snack', 'just water'), f"The detail transaction {transaction_4} is different from the created.")
        self.assertEqual(transaction_5,(5,'bread', 100, 'bakery', 'make some snack', 'some description'), f"The detail transaction {transaction_5} is different from the created.")

    #@unittest.skip('not wip')
    def test_can_execute_update_transaction(self,db_name=SQLITE_TEST_DB_NAME):
        db.create_connection(db_name)
        db.create_user_account_tables(db_name)
        utr.create_five_transactions(db_name)

        updated_values_index_1 = ('shirt', 900,'store', 'to go party', 'some extra desprition')
        updated_values_index_4 = ('bottle', 450, 'bar', 'make another snack', 'just water')
        updated_values_index_5 = ('bread', 200, 'bakery', 'make another snack', 'some description')

        db.execute_update_query(db_name, 1,  updated_values_index_1)
        db.execute_update_query(db_name, 4,  updated_values_index_4)
        db.execute_update_query(db_name, 5,  updated_values_index_5)

        transaction_1 = db.execute_detail_query(db_name, 1)
        transaction_4 = db.execute_detail_query(db_name, 4)
        transaction_5 = db.execute_detail_query(db_name, 5)

        self.assertEqual(transaction_1, (1, 'shirt', 900,'store', 'to go party', 'some extra desprition'), f"The detail transaction {transaction_1} is different from the updated.")
        self.assertEqual(transaction_4, (4, 'bottle', 450, 'bar', 'make another snack', 'just water'), f"The detail transaction {transaction_4} is different from the updated.")
        self.assertEqual(transaction_5, (5, 'bread', 200, 'bakery', 'make another snack', 'some description'), f"The detail transaction {transaction_5} is different from the updated.")


    #@unittest.skip('not wip')
    def test_can_execute_delete_transaction(self,db_name=SQLITE_TEST_DB_NAME):
        db.create_connection(db_name)
        db.create_user_account_tables(db_name)
        utr.create_five_transactions(db_name)

        transaction_2 = db.execute_detail_query(db_name, 2)
        transaction_4 = db.execute_detail_query(db_name, 3)

        db.execute_delete_query(db_name, 1)
        db.execute_delete_query(db_name, 4)
        db.execute_delete_query(db_name, 5)

        deleted_transaction = db.execute_detail_query(db_name, 5)

        transactions = db.execute_list_query(db_name, db.select_transactions, 'test_can_execute_list_of_transactions')
        number_of_transactions = len(transactions)

        self.assertEqual(number_of_transactions, 2, f"The number of transactions was 5, and 3 was delete. The result should be 2, not \"{number_of_transactions}\".")
        self.assertEqual(transaction_2[1], 'short', f"The title should be shirt.")
        self.assertEqual(transaction_4[2], 1500, f"The title should be shirt.")
        self.assertEqual(deleted_transaction, None, f"The title should be shirt.")


if __name__ == '__main__':
    unittest.main()

